import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { MDBAlert ,MDBContainer } from "mdbreact";
import '../forgotpass/forgot.css';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { Auth } from 'aws-amplify';
import ForgotPass from '../forgotpass/ForgotPass';
import {
	withRouter
} from 'react-router-dom';
import { isNull } from 'util';
import { red } from '@material-ui/core/colors';

class Login extends Component {

  handleAuthState(state) {
    this.setState({ authState: state });
    console.log("handleAuthState " + state);
  }


  routeChange() {
    let path = `forgotpass`;
    this.props.history.push(path);
  }

  constructor(props){
    super(props)
    this.state = {
      username:'',
      password:'',
      signedIn:false,
      error:'',
      uName:''
    }
    this.routeChange = this.routeChange.bind(this);
    this.handleChaneUser = this.handleChaneUser.bind(this);
    this.handleChanePass = this.handleChanePass.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.stateChange = this.handleAuthState.bind(this);
    Auth.currentSession()
      .then(user => this.stateChange('signedIn'))
      .catch(err => this.state = {authState: 'signIn'});
    this.state = {authState: 'signIn'};
  }

  handleChaneUser(e){
    this.setState({
      username : e.target.value
    });
  }
  handleChanePass(e){
    this.setState({
      password : e.target.value
    });
  }
  
  handleSubmit(e){
    e.preventDefault();
    const {signedIn,username,password} = this.state;
   

    if(!signedIn){
      Auth.signIn({
        username:username,
        password:password,
      }).then( () => this.props.history.push('/'),
      console.log('signedUp'))
      .catch(eror => { return this.errorVal(eror.message), console.log("signin error value --",eror.message)})

      this.setState({
        signedIn:true
      })
    }
    else{
      alert("user does not exist")
        // Auth.confirmSignIn(username)
        // .then(()=>console.log('confirm signUp',username))
        // .catch(error => console.log(error))

        // .username(username)
    }


  }

  errorVal(error){
      document.getElementById("error").innerHTML=error
  }

  componentDidMount(){
    Auth.currentAuthenticatedUser({
      bypassCache: false  // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
  }).then((user) => {
    return this.name(user.username),
    console.log("user val",user.username);
    
  })
   .catch(err => console.log(err));
  }

  forgot(){
    
  }
  name(name){
    console.log("name is",name)
    document.getElementById("demo").innerHTML=name

  }
  

  render() {

    const {signedUp,username} = this.state;
    console.log("name",username)
    if( this.state.authState == 'signedIn'){
      return <div style={{textAlign:"center", marginTop:20}}>
            <MDBContainer>
              <MDBAlert color="warning" dismiss>
                <strong id="demo">Hello </strong> You are allready loged in.
              </MDBAlert>
            </MDBContainer>
      </div>
    }
    {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                      <h1>Login</h1>
                      <h4 id="error" style={{color:red}} className="text-muted">Sign In to your account</h4>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                          {this.validation}
                        </InputGroupAddon>
                            <Input type="text" 
                            required name="username" 
                            value={this.state.username}
                            onChange={this.handleChaneUser} 
                            placeholder="Username" 
                            autoComplete="username" />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                            <Input type="password"
                            required name="password" 
                            onChange={this.handleChanePass} 
                            value={this.state.password}
                            placeholder="Password" 
                            autoComplete="current-password" />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" onClick={this.routeChange} className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <Link to="/register">
                        <Button color="primary" className="mt-3" active tabIndex={-1}>Register Now!</Button>
                      </Link>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
    }
  }
}

export default Login;
