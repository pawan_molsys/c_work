import React, { Component } from 'react';
import { Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { Auth } from 'aws-amplify';
import './style.css'
import {
	withRouter
} from 'react-router-dom';

class Register extends Component {

  constructor(props){
    super(props)
    this.state = {
      // username:'',
      // password:'',
      // email: '',
      // phone_number:'',
      // confirmationCode:'',
      // signedUp:false,
      fields: {},
        errors: {}
    }

    // this.handleChaneUser = this.handleChaneUser.bind(this);
    // this.handleChaneEmail = this.handleChaneEmail.bind(this);
    // this.handleChanePass = this.handleChanePass.bind(this);
    // this.handleChanePhone = this.handleChanePhone.bind(this);
    // this.handleChaneConfirm = this.handleChaneConfirm.bind(this);
    this.handleChane= this.handleChane.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // handleChaneUser(e){
  //   this.setState({
  //     username : e.target.value
  //   });
  // }
  // handleChanePass(e){
  //   this.setState({
  //     password: e.target.value
  //   });
  // }
  // handleChaneEmail(e){
  //   this.setState({
  //     email : e.target.value
  //   });
  // }
  // handleChanePhone(e){
  //   this.setState({
  //     phone_number : e.target.value
  //   });
  // }
  // handleChaneConfirm(e){
  //   this.setState({
  //     confirmationCode : e.target.value
  //   });
  // }
  

  handleChane(e){
    let fields = this.state.fields;
      fields[e.target.name] = e.target.value;
      this.setState({
        fields
      });

  }
  
  handleSubmit(e){
    e.preventDefault();
    const {signedUp,username,password,phone_number,email,confirmationCode} = this.state;
    var phoneNum = /^(\+91)\d\d\d\d\d\d\d\d\d\d/;
    var mailformat = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 

    if (this.validateForm()) {
      let fields = {};
      fields["username"] = "";
      fields["email"] = "";
      fields["phone_number"] = "";
      fields["password"] = "";
      this.setState({fields:fields});
      alert("Form submitted");
 

  

    // if(this.state.password<=8){
    //     alert("password should be greater then 8")
    // }
    // else if(!this.state.email.match(mailformat)){
    //     alert("You have entered an invalid email address ! format - example@gmail.com")
    // }
    // else if(!this.state.phone_number.match(phoneNum)){
    //   alert("valid format is +918100027000");
    // }
    // else{
      
    if(!signedUp){
      Auth.signUp({
        username:fields,
        password:password,
        attributes:{
          email:email,
          phone_number:phone_number
        }
      }).then( () => console.log('signedUp'))
      .catch(eror => console.log(eror))

      this.setState({
        signedUp:true
      })
    }
    else{
      if(!this.state.confirmationCode==6){
          alert("type the confirmation code")
      }
        Auth.confirmSignUp(username,confirmationCode)
        .then(()=> this.props.history.push('/login'),
        console.log('confirm signup',username))
        .catch(error => console.log(error))
    // }
  }
}

  }



  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;
    var phoneNum = /^(\+91)\d\d\d\d\d\d\d\d\d\d/;

    if (!fields["username"]) {
      formIsValid = false;
      errors["username"] = "*Please enter your username.";
    }

    if (typeof fields["username"] !== "undefined") {
      if (!fields["username"].match(/^[a-zA-Z ]*$/)) {
        formIsValid = false;
        errors["username"] = "*Please enter alphabet characters only.";
      }
    }

    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "*Please enter your email-ID.";
    }

    if (typeof fields["email"] !== "undefined") {
      //regular expression for email validation
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(fields["email"])) {
        formIsValid = false;
        errors["email"] = "*Please enter valid email-ID.";
      }
    }

    if (!fields["phone_number"]) {
      formIsValid = false;
      errors["phone_number"] = "*Please enter your mobile no.";
    }

    if (typeof fields["phone_number"] !== "undefined") {
      if (!fields["phone_number"].match(phoneNum)) {
        formIsValid = false;
        errors["phone_number"] = "*Please enter valid mobile no.";
      }
    }

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "*Please enter your password.";
    }

    if (typeof fields["password"] !== "undefined") {
      if (!fields["password"].match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {
        formIsValid = false;
        errors["password"] = "*Please enter secure and strong password.";
      }
    }

    this.setState({
      errors: errors
    });
    return formIsValid;


  }



  render() {

    const {signedUp} = this.state;
    
    if(signedUp){
      return (
        <div className="app flex-row align-items-center">
      <Container>
        <Row className="justify-content-center">
          <Col md="9" lg="7" xl="6">
            <Card className="mx-4">
              <CardBody className="p-4">
                <Form onSubmit={this.handleSubmit}>
                  <h1>Register</h1>
                  <p className="text-muted">Create your account</p>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-user"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" 
                    name="username" 
                    value={this.state.username}
                    onChange={this.handleChaneUser} placeholder="Username"
                     autoComplete="username" />
                     
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>@</InputGroupText>
                    </InputGroupAddon>
                    <Input type="number" 
                    name="confirmationCode" 
                    value={this.state.confirmationCode}
                    onChange={this.handleChaneConfirm} 
                    placeholder="confirmation code"
                     autoComplete="confirmation code" />
                  </InputGroup>
                  
                  <Button color="success"  block>confirm</Button>
                </Form>
              </CardBody>
             
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
      )
    }
    {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="9" lg="7" xl="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form onSubmit={this.handleSubmit}>
                    <h1>Register</h1>
                    <p className="text-muted">Create your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" 
                      name="username" 
                      value={this.state.fields.username}
                      onChange={this.handleChane} 
                      placeholder="Username"
                       autoComplete="username" />
                       <div className="errorMsg">{this.state.errors.username}</div>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" 
                      
                      name="email" 
                      value={this.state.fields.email}
                      onChange={this.handleChane} 
                      placeholder="Email" 
                      autoComplete="email" />
                      <div className="errorMsg">{this.state.errors.email}</div>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" 
                     
                      name="password" 
                      value={this.state.fields.password}
                      onChange={this.handleChane} 
                      placeholder="Password" 
                      autoComplete="new-password" />
                      <div className="errorMsg">{this.state.errors.password}</div>
                    </InputGroup>
                   
                    <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-phone"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" 
                       
                       name="phone_number"  
                       value={this.state.fields.phone_number}
                       onChange={this.handleChane}
                        placeholder="Ph Number" />
                        <label className="errorMsg">{this.state.errors.phone_number}</label>
                        {/* <div className="errorMsg">{this.state.errors.phone_number}</div> */}
                    </InputGroup>

                    <Button color="success" style={{marginTop:20}} block>Create Account</Button>
                  </Form>
                </CardBody>
                <CardFooter className="p-4">
                  <Row>
                    <Col xs="12" sm="6">
                      <Button className="btn-facebook mb-1" block><span>facebook</span></Button>
                    </Col>
                    <Col xs="12" sm="6">
                      <Button className="btn-twitter mb-1" block><span>twitter</span></Button>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
    }
  }
}

export default Register;
