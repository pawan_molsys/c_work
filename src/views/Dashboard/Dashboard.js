import React, { Component} from 'react';
import { Slide } from 'react-slideshow-image';
import hot_desk from '../../assets/img/brand/hot_desk.jpg';
import dedicated_desk from '../../assets/img/brand/dedicated_desk.jpg';
import private_cabin from '../../assets/img/brand/private_cabin.jpg';
import private_office from '../../assets/img/brand/private_office.jpg';
import conference from '../../assets/img/brand/conference_room.jpg'
import { Card } from 'semantic-ui-react'
import Service from './Service';
import BookSpace from './BookSpace';
import Tab from '../../views/Base/Tabs/Tabs';
import Ad_on from './Ad_one';
import Daynamic from  './Daynamic';
const slideImages = [
  hot_desk,
  dedicated_desk,
  private_cabin,
  private_office,
  conference
];

var bgColors = { "Default": "#81b71a",
                    "Blue": "#00B1E1",
                    "Cyan": "#37BC9B",
                    "Green": "#8CC152",
                    "Red": "#E9573F",
                    "Yellow": "#F6BB42",
                    "white" : "#FFFFFF",
};
 
const properties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  scale: 0.4,
  arrows: true
}

var style = {
  color: 'white',
  fontSize: 200,
  float:'right'
};


class Dashboard extends Component{ 


  constructor(props){
    super(props);
    this.state = {
      news:[],
    };
  }


  componentDidMount() {

    const  url = 'https://newsapi.org/v2/top-headlines?country=us&apiKey=d5cf45043cd34b59b432df10e3cef274';
   
   
     fetch(url)
       .then((response) => {
         return response.json();
       })
       .then((data) => {
         this.setState({
           news: data.articles
         })
         console.log(data);
       })
       .catch((error)=>{
         console.log('error while trying to retrieve data')
       })
   }
   
   renderItems(){
     const src = 'https://placeimg.com/640/480/arch'
     return this.state.news.map((item) =>(
       <Card.Group style={{width:1000}}>
         <Card
           image={src}
           header='Elliot Baker'
           meta='Friend'
           description='Elliot is a sound engineer living in Nashville who enjoys playing guitar and hanging with his cat.'
         />
        
       </Card.Group>
   
     ));
   }


  render(){

  

    return (
      <div>
      <Slide {...properties} >
        <div className="each-slide" >
          <div style={{color:'white',height:400,'backgroundImage': `url(${slideImages[0]})`}}>
            <span style={{border:1, textAlign:"center", marginTop:300}}>Slide 1</span>
          </div>
        </div>
        <div className="each-slide" >
          <div style={{color:'white',height:400,'backgroundImage': `url(${slideImages[1]})`}}>
            <span >Slide 2</span>
          </div>
        </div>
        <div className="each-slide" >
          <div style={{color:'white', height:400,'backgroundImage': `url(${slideImages[2]})`}}>
            <span >Slide 3</span>
          </div>
        </div>
        <div className="each-slide" >
          <div style={{height:400,'backgroundImage': `url(${slideImages[3]})`}}>
            <span>Slide 3</span>
          </div>
        </div>
        <div className="each-slide" >
          <div  style={{height:400,'backgroundImage': `url(${slideImages[4]})`}}>
            <span style={{style}}>Slide 3</span>
            
          </div>
        </div>
      </Slide>

     
      <div style={{marginBottom:10}}>
      <Service />
      </div>
      
      <div style={{marginBottom:10}}>
          <BookSpace />
      </div>

      <Daynamic/>
       
      <Tab/>

      <Ad_on/>

      </div>
    );
  
  }   
}
export default Dashboard;
