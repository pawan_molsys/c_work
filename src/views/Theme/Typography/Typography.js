import React, { Component } from 'react';
import { MDBContainer, MDBAlert } from 'mdbreact';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import dedicated_desk from '../../../assets/img/brand/dedicated_desk.jpg'


const styles = {
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
};


function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class Typography extends Component {
  
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        
      <section className="text-center my-5">
      <MDBContainer>
        <MDBAlert color="success">
          <h4 className="alert-heading">Dedicated Desk!</h4>
          <p>Dedicated desk
                - If you are a person of habit or you want to choose a
                desk   well-suited   to   you,   we   also   offer   an   option   of   selecting   your   own
                desk. When you start your journey with us, you can come in and select a
                desk to your liking and we shall assign it to you.</p>
                 <hr />
                <p className="mb-0">Facilities:
                       Wi-fi service, house-keeping, filing cabinet (with a lock)</p>
        </MDBAlert>
      </MDBContainer>
                <div class="row">
              <div class="col-sm-6">
                <div class="card" >
                <img class="card-img-top" src={dedicated_desk} alt="Card image cap"/>
                  <div class="card-body">
                    <h5 class="card-title">Dedicated Desk</h5>
                    <p class="card-text">Reserved Seat</p>
                    <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
                       Book the seat
                    </Button>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card" >
                <img class="card-img-top" src={dedicated_desk} alt="Card image cap"/>
                  <div class="card-body">
                    <h5 class="card-title">Dedicated Desk</h5>
                    <p class="card-text">Choose Your Own Seat </p>
                    <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
                       Book the seat
                    </Button>
                  </div>
                </div>
              </div>
            </div>

          {/* dialog part ............................................................. */}

          <Dialog
          style={{margin:100}}
          open={this.state.open}
          onClose={this.handleClose}
          TransitionComponent={Transition}
        >
          {/* <AppBar>
            <Toolbar>
              <IconButton color="inherit" onClick={this.handleClose} aria-label="Close" >
                <CloseIcon />
              </IconButton>
              <Typography1 variant="h6" color="inherit" >
               
              </Typography1>
              <Button color="inherit" onClick={this.handleClose}>
                save
              </Button>
            </Toolbar>
          </AppBar> */}
          <div className="modal-content">
              <div className="modal-header">
                       <h5 className="modal-title">Address : 01 Kogilu, Mittiganahalli cross Yelahanka, Bangalore 560064</h5>
               
              </div> 

              <div className="modal-header">
                  <h5>Tel : +91 9108896655 </h5>
              </div>   
              
          </div>
        </Dialog>

          </section>
  
  
      </div>
    );
  }
}


Typography.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Typography);

