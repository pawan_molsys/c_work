import React, { Component } from 'react';
import {Auth} from 'aws-amplify';


var dis = true ;

class _nav extends Component{
  
  constructor(props){
    super(props)

    this.state={
      tog:''
    }
}

async componentDidMount(){
  
  await  Auth.currentAuthenticatedUser({
        bypassCache: false  // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
    }).then(user => { return this.setState({tog:''}),console.log("inside ==",this.state.tog) })
    .catch(err => {
      return this.setState({ tog:'none'}),console.log("inside catch -- ",this.state.tog)});
    console.log("tog value-- ",this.state.tog);


    

}
  render(){
    return(
      <div>

      </div>
    )
  }
}



export default  {

  items: [
    
    {
      title: true,
      name: 'Desks',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Hot Desks',
      url: '/HotDesk',
      icon: 'icon-drop',
      attributes: { disabled: dis },
    },
    {
      name: 'Dedicated Desks',
      url: '/DedicatedDesks',
      icon: 'icon-pencil',
      attributes: { disabled: dis },
    },
    {
      title: true,
      name: 'private',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Private',
      url: '/base',
      icon: 'icon-puzzle',
      children: [
        {
          name: 'Private Cabin',
          url: '/PrivateCabin',
          icon: 'icon-puzzle',
          attributes: { disabled: dis },
        },
        {
          name: 'Private Office',
          url: '/PrivateOffices',
          icon: 'icon-puzzle',
          attributes: { disabled: dis },
          
        }
      ],
    },
    {
      title: true,
      name: 'SignIn & SignUp',
    },
    {
      name: 'SignIn & SignUp',
      url: '/pages',
      icon: 'icon-star',
      children: [
        {
          name: 'Login',
          url: '/login',
          icon: 'icon-star',
        },
        {
          name: 'Register',
          url: '/register',
          icon: 'icon-star',
        },
      ],
    },
    {
      name: 'Disabled',
      url: '/dashboard',
      icon: 'icon-ban',
      attributes: { disabled: true },
    },
    
    
  ],
};
