import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ButtonBase from '@material-ui/core/ButtonBase';
import Typography from '@material-ui/core/Typography';
import slide0 from '../../assets/img/brand/slide.jpg'
import slide1 from '../../assets/img/brand/slide1.jpg'
import slide2 from '../../assets/img/brand/slide2.jpg'
import slide3 from '../../assets/img/brand/slide3.jpg'
import slide4 from '../../assets/img/brand/slide3.jpg'

import hot_desk from '../../assets/img/brand/hot_desk.jpg';
import dedicated_desk from '../../assets/img/brand/dedicated_desk.jpg';
import private_cabin from '../../assets/img/brand/private_cabin.jpg';
import private_office from '../../assets/img/brand/private_office.jpg';
import conference from '../../assets/img/brand/conference_room.jpg'

import PrivateCabin from '../Base/Breadcrumbs/Breadcrumbs'
import privateOffice from '../Base/Cards/Cards'
import { Link } from 'react-router-dom'
import linkcss from '../Dashboard/service.css'
import { NavLink } from 'react-router-dom';
import { Card, CardHeader, ImageHeader,CardBody, CardFooter } from "react-simple-card";
import Button from '@material-ui/core/Button';
import {  MDBRow, MDBCol, MDBCardBody, MDBIcon, MDBBtn, MDBView, MDBMask } from "mdbreact";
import route from '../../routes'
import { Auth } from 'aws-amplify';


const styles = theme => ({

  root: {
    display: 'flex',
    flexWrap: 'wrap',
    minWidth: 300,
    width: '100%',
  },
  image: {
    position: 'relative',
    height: 200,
    [theme.breakpoints.down('xs')]: {
      width: '100% !important', // Overrides inline-style
      height: 100,
    },
    '&:hover, &$focusVisible': {
      zIndex: 1,
      '& $imageBackdrop': {
        opacity: 0.15,
      },
      '& $imageMarked': {
        opacity: 0,
      },
      '& $imageTitle': {
        border: '4px solid currentColor',
      },
    },
  },
  focusVisible: {},
  imageButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.common.white,
  },
  imageSrc: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundPosition: 'center 40%',
  },
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: theme.palette.common.black,
    opacity: 0.4,
    transition: theme.transitions.create('opacity'),
  },
  imageTitle: {
    position: 'relative',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 4}px ${theme.spacing.unit + 6}px`,
  },
  imageMarked: {
    height: 3,
    width: 18,
    backgroundColor: theme.palette.common.white,
    position: 'absolute',
    bottom: -2,
    left: 'calc(50% - 9px)',
    transition: theme.transitions.create('opacity'),
  },
});




class Service extends Component {

  constructor(props){
      super(props)

      this.state={
        tog:''
      }
  }

 async componentDidMount(){
    
    await  Auth.currentAuthenticatedUser({
          bypassCache: false  // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
      }).then(user => { return this.setState({tog:''}),console.log("inside ==",this.state.tog) })
      .catch(err => {
        return this.setState({ tog:'none'}),console.log("inside catch -- ",this.state.tog)});
      console.log("tog value-- ",this.state.tog);

  }
 
  render(){
    
  return (
    <div className="container">
      

          <section className="text-center my-5">
        <h2 className="h1-responsive font-weight-bold my-5">
            WE OFFER
        </h2>
        <p className="grey-text w-responsive mx-auto mb-5">
          The best in class serviced co-working spaces and business ecosystems for startups
          and small companies to incubate. With good ambience, location and customizable spaces 
          build your own workspace.  
        </p>

        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="6" xl="5" className="mb-4">
            <MDBView className="overlay rounded z-depth-2" waves>
              <img
                src={hot_desk}
                alt=""
                className="img-fluid"
              />
              <a href="">
                <MDBMask overlay="white-slight" />
              </a>
            </MDBView>
            <MDBCardBody className="pb-0">
              <Link to="/HotDesk" style={{pointerEvents:this.state.tog}}>
                <h5 className="font-weight-bold mt-2 mb-3" className={this.state.tog}>
                 
                  Hot Desk
                </h5>
                </Link>
           
            </MDBCardBody>
          </MDBCol>
          <MDBCol md="6" xl="5" className="mb-4">
            <MDBView className="overlay rounded z-depth-2" waves>
              <img
                src={dedicated_desk}
                alt=""
                className="img-fluid"
              />
              <a href="#!">
                <MDBMask overlay="white-slight" />
              </a>
            </MDBView>
            <MDBCardBody className="pb-0">
            <Link to="/DedicatedDesks" style={{pointerEvents:this.state.tog}}>
                <h5 className="font-weight-bold mt-2 mb-3">
                  
                    Dedicated Desk
                </h5>
              </Link>
            
            </MDBCardBody>
          </MDBCol>
          <MDBCol md="6" xl="5" className="mb-4">
            <MDBView className="overlay rounded z-depth-2" waves>
              <img
                src={private_cabin}
                alt=""
                className="img-fluid"
              />
              <a href="#!">
                <MDBMask overlay="white-slight" />
              </a>
            </MDBView>
            <MDBCardBody className="pb-0">
            <Link to="/PrivateCabin" style={{pointerEvents:this.state.tog}} >
                <h5 className="font-weight-bold mt-2 mb-3">
                 
                 Private Cabin
                </h5>
              </Link>
            
            </MDBCardBody>
          </MDBCol>
          <MDBCol md="6" xl="5" className="mb-4">
            <MDBView className="overlay rounded z-depth-2" waves>
              <img
                src={private_office}
                alt=""
                className="img-fluid"
              />
              <a href="#!">
                <MDBMask overlay="white-slight" />
              </a>
            </MDBView>
            <MDBCardBody className="pb-0">
            <Link to="/PrivateOffices" style={{pointerEvents:this.state.tog}}>
                <h5 className="font-weight-bold mt-2 mb-3">
                  
                  Private Office
                </h5>
              </Link>
           
            </MDBCardBody>
          </MDBCol>

          <MDBCol md="6" xl="5" className="mb-4">
            <MDBView className="overlay rounded z-depth-2" waves>
              <img
                src={conference}
                alt=""
                className="img-fluid"
              />
              <a href="#!">
                <MDBMask overlay="white-slight" />
              </a>
            </MDBView>
            <MDBCardBody className="pb-0">
            <Link to="/" style={{pointerEvents:this.state.tog}}>
                <h5 className="font-weight-bold mt-2 mb-3">
                  {/* <MDBIcon icon="phone" className="pr-2" /> */}
                  Conference Room
                </h5>
              </Link>
              
            </MDBCardBody>
          </MDBCol>

        </MDBRow>
      </section>


    

         
    </div>
  );
}
}

Service.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Service);