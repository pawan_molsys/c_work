import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';

import private_cabin from '../../../assets/img/brand/private_cabin.jpg';

// import { Grid, Card, Icon, Image , Button, Item} from 'semantic-ui-react'

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class Breadcrumbs extends Component{
  // const { classes } = props;

  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render(){
    return (
      <div className="card card-cascade wider reverse">

   
    <div className="view view-cascade overlay" > 
      <img className="card-img-top" src={private_cabin} alt="Card image cap" />
      <a href="#!">
        <div className="mask rgba-white-slight"></div>
      </a>
    </div>

 
    <div className="card-body card-body-cascade text-center">

   
      <h4 className="card-title"><strong>Private Cabin</strong></h4>

      <h6 className="font-weight-bold indigo-text py-2">Facilities: Room size 10*6 ft, AC, Wifi, Housekeeping</h6>
 
      <p className="card-text">
          Have your own space for two persons seating. Private cabin shall be open for your client
          interactions and confidential meetings
      </p>

      <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
                   Book now
                </Button>
      

    </div>

    <Dialog
      style={{margin:100}}
      open={this.state.open}
      onClose={this.handleClose}
      TransitionComponent={Transition}
    >
    
      <div className="modal-content">
          <div className="modal-header">
                   <h5 className="modal-title">Address : 01 Kogilu, Mittiganahalli cross Yelahanka, Bangalore 560064</h5>
           
          </div> 

          <div className="modal-header">
              <h5>Tel : +91 9108896655 </h5>
          </div>   
          
      </div>
    </Dialog>


  </div>
);
  }

  
}

export default Breadcrumbs;

