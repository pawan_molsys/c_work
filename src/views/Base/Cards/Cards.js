import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContent';
import private_office from '../../../assets/img/brand/private_office.jpg';
import style1 from './privateoffice.css';



const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    // width: 500,
    // height: 450,
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
});

    const tileData = [
      {
        img: private_office,
        title: 'private office',
        author: '5 seater w/o/AC'
      },
      {
        img: private_office,
        title: 'private office',
        author: '4 seater (meeting room availabe for a month)'
      },
      {
        img: private_office,
        title: 'private4',
        author: '5 seater w/AC'
      },
      {
        img: private_office,
        title: 'private office',
        author: '8 seater w/AC'
      }
    ]

    


function Card(props) {
  
  const { classes } = props;
  
  const [open, setOpen] = React.useState(false);

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }


  return (
    <div className={classes.root} style={{marginBottom:10}}>
      <GridList  className={classes.gridList}>
        <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
          <ListSubheader component="div">Private offices:
              Enclosed furnished office area ideal for 5-8 working members</ListSubheader>
        </GridListTile>
        {tileData.map(tile => (
          <GridListTile key={tile.img}>
            <img src={tile.img} alt={tile.title} />
            <GridListTileBar
              title={tile.title}
               subtitle={<span> {tile.author}</span>}
              actionIcon={
                // <IconButton className={classes.icon} >
                      <div className={style1.private}>
                      <button onClick={handleClickOpen} className={style1.button}>Book now</button>
                      </div>
                // </IconButton>
              }
            />
          </GridListTile>
        ))}
      </GridList>


      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        {/* <DialogTitle id="form-dialog-title">Subscribe</DialogTitle> */}
        <DialogContent>
          <DialogContentText>
          <div className="modal-content">
              <div className="modal-header">
                       <h5 className="modal-title">Address : 01 Kogilu, Mittiganahalli cross Yelahanka, Bangalore 560064</h5>
              </div> 

              <div className="modal-header">
                  <h5>Tel : +91 9108896655 </h5>
              </div>   
              
          </div>
          </DialogContentText>
        </DialogContent>
        {/* <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleClose} color="primary">
            Subscribe
          </Button>
        </DialogActions> */}
      </Dialog>

    </div>
  );
}

Card.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Card);

