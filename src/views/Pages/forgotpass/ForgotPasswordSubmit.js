import React, { Component } from "react";
import {Auth, Logger} from 'aws-amplify';
import './forgot.css'
import { red} from "@material-ui/core/colors";
const logger = new Logger('ForgotPassword');

class ForgotPasswordSubmit extends Component{
  constructor(props){
    super(props)
    this.state ={
      userName:'',
      code:'',
      newPass:'',
      error:''
    }
    this.onchangeUser=this.onchangeUser.bind(this);
    this.onChangeCod=this.onChangeCod.bind(this);
    this.onChangeNewpass=this.onChangeNewpass.bind(this);
    this.onSubmit=this.onSubmit.bind(this);
    
  }

  routeChange() {
    let path = `login`;
    this.props.history.push(path);
  }

  onchangeUser = (e) => {
    this.setState({
      userName:e.target.value
    })
  }

  onChangeCod = (e) => {
    this.setState({
      code:e.target.value
    })
  }

  onChangeNewpass = (e) => {
    this.setState({
      newPass:e.target.value
    })
  }
  
  

  onSubmit = async (e) => {
    e.preventDefault();
    console.log(this.state.userName,this.state.newPass,this.state.code)
   await Auth.forgotPasswordSubmit(this.state.userName, this.state.code, this.state.newPass)
    .then(data => {
      return this.routeChange(),window.confirm("Your password has been sucessfully changed");
      })
    .catch(err => {
      if(this.state.userName==""){
          return this.showErr(err), console.log("error",err)
      }
      if(this.state.code==""){
        return this.showErr(err), console.log("error",err)
      }
      if(this.state.newPass==""){
        return this.showErr(err), console.log("error",err)
      }

      if(!this.state.userName||this.state.code||this.state.error==""){
          return this.showErr(err.message),console.log(err);
      }

    //   if(this.state.userName==""){
    //       return this.showErr(err)
         
    //   }
    //   if(this.state.code==""){
    //      return this.showErr(err)
    //   }
    //  if(this.state.newPass==""){
    //     return this.showErr(err)
    //   }
     
    });

  }

  showErr(error){
    
    document.getElementById("err").innerHTML=error
  }

    render(){
        return(
            <div className="jumbotron text-center">
                     <h4 id="err" style={{color:red}}>varification code is sent in your register email</h4>
                  <form className="text-center border border-light p-5" onSubmit={this.onSubmit}>

                  <input type="text" name="userName" value={this.state.userName} onChange={this.onchangeUser} className="form-control mb-4" placeholder="userName"/>
                   
                  <input type="text" name="code" value={this.state.code} onChange={this.onChangeCod} className="form-control mb-4" placeholder="verification code"/>
                   
                  <input type="password" name="newPass" value={this.state.newPass} onChange={this.onChangeNewpass} className="form-control mb-4" placeholder="New Password"/>
                   
                    {/* <span id="err" style={{color:red,marginBottom:40,backgroundColor:pink}}>
                    verification Code is sent in your register email</span> */}
                  
                  <input className="btn btn-lg btn-primary btn-block" value="SUBMIT" type="submit"/>

               </form>


            </div>
        )
    }
}
export default ForgotPasswordSubmit