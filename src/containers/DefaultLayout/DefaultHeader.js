import React, { Component } from 'react';
import { Link, NavLink} from 'react-router-dom';
import { DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem} from 'reactstrap';
import PropTypes from 'prop-types';
import {Auth,Hub} from 'aws-amplify';
import './default.css'

import { AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo.svg'
import sygnet from '../../assets/img/brand/sygnet.svg'
import Register from '../../views/Pages/Register/Register';


const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {

    constructor(props){
      super(props)
      console.log("inside constructor",this.props)
    }
    componentDidMount() {
      Hub.listen('auth', function(authData) {
        console.log('authData: ', authData)
        if (authData.payload.event === 'signOut') {
          let x = authData.payload.event
         
          console.log("length--",x.length);
          
         
          alert(authData.payload.event);
          // this.props.history.push("/login");
        }
      })
    }
  
   sign_out(){

    Auth.signOut({ global: true })
    .then((data)=> console.log("signout"))
    .catch(err => console.log("signout error --",err));

    // Hub.listen('auth', function(authData) {
    //   console.log('authData: ', authData)
    //   if (authData.payload.event === 'signOut') {
    //     let count = authData.payload.event;
    //     let countc;
    //      for(let i=0;i<count;i++){
    //           countc = countc+count
    //           console.log("counting value --",countc)
    //      }
    //    alert(authData.payload.event);
    //   //  window.location.reload();
    //   }
      
    // })

  }

 
  signoutuser(data){
    console.log("message - ",data);
    document.getElementById("demo").innerHTML=data;
  }

  render() {
  
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;
    
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'CoreUI Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
        />
        <img src=""></img>
        <AppSidebarToggler className="d-md-down-none" />
        <Nav className="ml-auto" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link" >Product & Service</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/about" className="nav-link">AboutUs</Link>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="/contact" className="nav-link">Contact</NavLink>
          </NavItem>
         
         {/* <NavItem className="px-3">
             <NavLink className="nav-link" id="demo"> </NavLink>
         </NavItem> */}
        
          {/* <NavItem className="px-3" > */}

          {/* <button type="button"onClick={this.sign_out} id="message" class="btn btn-outline-default btn-rounded waves-effect"></button> */}
            {/* <button onClick={this.sign_out} id="message">signout</button> */}
            {/* <span id="message"></span> */}
            {/* <button className="favorite styled" onClick={this.sign_out} id="message" 
                 type="button">
                    logout
            </button> */}
          {/* <SignOut/> */}
          {/* </NavItem> */}
           
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img src={'../../assets/img/avatars/6.jpg'} className="img-avatar" alt="example@gmail.com" />
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
              <DropdownItem onClick={this.sign_out}  ><i className="fa fa-lock" ></i> Logout</DropdownItem>
              <DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem>    
            </DropdownMenu>
          </AppHeaderDropdown>

        </Nav>

        
       
      </React.Fragment>
    );
  }
}

class AuthWrapper extends React.Component {
  rerender = () => this.forceUpdate()
  render() {
    return <Register rerender={this.rerender} />
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
