import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './App.scss';
import Amplify from 'aws-amplify';
import awsmobile from './aws-exports';
import { withAuthenticator } from 'aws-amplify-react'; // or 'aws-amplify-react-native';
import { ForgotPassword } from 'aws-amplify-react/dist/Auth';
Amplify.Logger.LOG_LEVEL = 'DEBUG';
Amplify.configure(awsmobile);




const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));
const DefaultLayoutHeader = React.lazy(() => import('./containers/DefaultLayout/DefaultHeader'));

// Pages
const Login = React.lazy(() => import('./views/Pages/Login'));
const Register = React.lazy(() => import('./views/Pages/Register'));
const Page404 = React.lazy(() => import('./views/Pages/Page404'));
const Page500 = React.lazy(() => import('./views/Pages/Page500'));
const Forgot = React.lazy(() => import('./views/Pages/forgotpass/ForgotPass'));
const Service = React.lazy(() => import('./views/Dashboard/Service'));
const Nav = React.lazy(() => import('./_nav'))

class App extends Component {

  render() {
    return (
      <div >
         <header className="App-header">
      
      <HashRouter>
          <React.Suspense fallback={loading()}>
            <Switch>
              <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
              <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
              <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
              <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
              <Route path="/" name="Home" render={props => <DefaultLayout {...props}/>} />
              <Route exact path="/forgotpass" name="forgotPass" render={props => <Forgot {...props}/>} />
              <Route exact path="/header" name="header" render={props => <DefaultLayoutHeader {...props}/>} />
              <Route exact path="/service" name="nav" render={props => <Service {...props}/>}/>
            </Switch>
          </React.Suspense>
      </HashRouter>
      </header>
      </div>
    );
  }
}

export default App;

