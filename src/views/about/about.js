import React, { Component } from 'react';
import slide2 from '../../assets/img/brand/slide3.jpg'
import PhotoGrid from 'react-photos-grid'
class About extends Component {
  constructor(props) {
        super(props);

        this.state = {
            images: this.props.images
        };
}
  render() {
    
    return (

        <div>
          <div className="text-center">
             <h2 className="h1-responsive font-weight-bold text-center ">About us</h2>
             <hr></hr>
             <h2  style={{color:'blue'}} className="h1-responsive font-weight-bold text-center my-5">Transform your business</h2>
             <p className="text-center w-responsive mx-auto pb-5">Get your own collaborative workspace at the most affordable pricing in the city. </p>
              <hr></hr>
             <h2  style={{color:'blue'}} className="h1-responsive font-weight-bold text-center my-5">Fully customizable spaces</h2>
             <p className="text-center w-responsive mx-auto pb-5">Lets have it suit your needs to make your workplace get the best out of you.</p>
              <hr></hr>
             <h2  style={{color:'blue'}} className="h1-responsive font-weight-bold text-center my-5">Private offices to hot desks</h2>
             <p className="text-center w-responsive mx-auto pb-5">We provide private office space to hot desks starting from as low as INR 3000 pm*.</p>
          </div>

          <hr></hr>
           <h3 className="text-center" className="h1-responsive font-weight-bold text-center my-5" >Meet our Team</h3> 
         
          <div className="row">
            <div className="column"> 
              <div className="card">
                <img src={slide2} alt="Jane" style={{"height" : "50%", "width" : "100%"}}/>
                <div className="container">
                  <h2>Jane Doe</h2>
                  <p className="title">CEO & Founder</p>
                  <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                  <p>example@example.com</p>
                  <p><button className="button" >Contact</button></p>
                </div>
              </div>
            </div>

            <div className="column">
              <div className="card">
                <img src={slide2} alt="Jane" style={{"height" : "50%", "width" : "100%"}}/>
                <div className="container">
                  <h2>Mike Ross</h2>
                  <p className="title">Art Director</p>
                  <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                  <p>example@example.com</p>
                  <p><button className="button">Contact</button></p>
                </div>
              </div>
            </div>
            <div className="column">
              <div className="card">
                <img src={slide2} alt="Jane" style={{"height" : "50%", "width" : "100%"}}/>
                <div className="container">
                  <h2>John Doe</h2>
                  <p className="title">Designer</p>
                  <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                  <p>example@example.com</p>
                  <p><button className="button">Contact</button></p>
                </div>
              </div>
            </div>
          </div>
      </div>
   
    )
  }
}

export default About;
