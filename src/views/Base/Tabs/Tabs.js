import React, {Component} from 'react';
import {Badge, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane} from 'reactstrap';
import classnames from 'classnames';
import tab from './tab.css'

class Tabs extends Component {


  render() {
    
    return (
      
      <div className="center" align="center">
       
        <h3>Services</h3>
        <hr style={{width:150}}></hr>
        <Row>
          <Col xs="12" md="4" className="mb-4">
          <img src="https://img.icons8.com/ios/100/000000/timer-filled.png"/>
          <h6 >9AM to 8PM</h6>
        </Col>
          <Col>
          <img src="https://img.icons8.com/ios/100/000000/internet-filled.png"/>
          <h6>Superfast Internet </h6>
          </Col>
          <Col xs="12" md="4" className="mb-4">
          <img src="https://img.icons8.com/wired/100/000000/organization.png"/>
          <h6>Company Registartion</h6>
          </Col>
          <Col xs="12" md="4" className="mb-4">
          <img src="https://img.icons8.com/ios/100/000000/coffee-pot-filled.png"/>
          <h6>Cafeteria</h6>
          </Col>
          <Col xs="12" md="4" className="mb-4">
          <img src="https://img.icons8.com/ios/100/000000/security-checked-filled.png"/>
          <h6>Securiy</h6>
          </Col>
          <Col>
          <img src="https://img.icons8.com/ios/100/000000/housekeeping-filled.png"/>
          <h6>HouseKeeping</h6>
          </Col>
        </Row>
        </div>
    
    );
  }
}

export default Tabs;
