import React, { Component } from "react";
import { MDBInput} from 'mdbreact';
import { Auth } from 'aws-amplify';
import './forgot.css'


class ForgotPass extends Component {
    constructor(props){
        super(props)
        this.state = {
            username:'',
            error:''
        }
        this.onHandle = this.onHandle.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    handleAuthState(state) {
        this.setState({ authState: state });
        console.log("handleAuthState " + state);
      }
    
    
      routeChange() {
        let path = `forgotpasssubmit`;
        this.props.history.push(path);
      }

    onHandle = (e) =>{

        this.setState({
            username:e.target.value
        })
        
    }

    onSubmit = async (e) =>{
        e.preventDefault();
        if(this.state.username=="undefined"&&this.state.username==""){
           
            return console.log("error")
        }
        else{
           await Auth.forgotPassword(this.state.username)
             .then(data =>{
                 return this.routeChange(), console.log("forgotpas---",data.CodeDeliveryDetails)
             } )
                .catch(err => {
                    console.log("before if con ", err)
                    if(this.state.username==""){
                        console.log("before if con ", err)
                        return this.errorshow(err),console.log("error value --",err)
                    }
                    else{
                        return this.errorshow(err.message);
                    }
                  
                });
        }
        
    }
    errorshow(error){
       
        document.getElementById("demo").innerHTML=error;
    }

    render(){
        return (
            <div className="jumbotron text-center">
                    <h3><i className="fa fa-lock fa-4x"></i></h3>
                    <h2 className="text-center">Forgot Password?</h2>
                    <p>You can reset your password here.</p>        
                    <form className="form" onSubmit={this.onSubmit}> 
                    <div className="form-group" style={{marginLeft:200,marginRight:200,marginTop:30}}>
                    <MDBInput
                        onChange={this.onHandle}
                        value={this.state.username}
                        name="username"
                        hint="UserName " 
                        type="text" />
                    </div>
                    <span id="demo"></span>
                    <div style={{ marginLeft:200,marginRight:200,marginTop:30}}>
                        <input className="btn btn-lg btn-primary btn-block" value="Next" type="submit"/>
                    </div>
                    </form>   
            </div>
          );
    }

  
};

export default ForgotPass;