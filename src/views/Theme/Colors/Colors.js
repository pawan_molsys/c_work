import React, { Component } from 'react';
import { MDBContainer, MDBAlert } from 'mdbreact';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import hot_desk from '../../../assets/img/brand/hot_desk.jpg'


function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class Colors extends Component {

  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        
    <section className="text-center my-5">
    <MDBContainer>
      <MDBAlert color="success">
        <h4 className="alert-heading">Hot Desk!</h4>
        <p>A shout-out to all the inspiring bloggers, freelancers, consultants, startups,
                    growing businesses and to anyone with a creative idea to implement. We
                    are offering shared work-space, specifically catered to your needs, at an
                    upcoming   and   a   convenient   location.   Located   just   15   min   away   from
                    Hebbal/Manyata   Tech   Park   and   20   min   from   KIA,   we   provide   secured
                    working platforms to ignite your mind, embolden your ideas, and improve
                    your productivity. 
                    Rent-a-desk or Desking options
                    Hot desk- 
                    We bring to you a trend of 1990s, a seating system available
                    on   first-come,  first-serve   basis.  You   can  simply  plug-in   your   laptops  and
                    start your work at any of the vacant desks available at the time of entry.
                    A new working spot for you every day. For more booking details, contact
                    here.</p>
              <hr />
              <p className="mb-0">Facilities:
                     Wi-fi service, house-keeping, </p>
      </MDBAlert>
    </MDBContainer>
              <div className="row">
            <div className="col-sm-6">
              <div className="card" >
              <img className="card-img-top" src={hot_desk} alt="Card image cap"/>
                <div className="card-body">
                  <h5 className="card-title">Hot Desk</h5>
                  <p className="card-text">Daily Basis</p>
                  <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
                       Book the seat
                    </Button>
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="card" >
              <img className="card-img-top" src={hot_desk} alt="Card image cap"/>
                <div className="card-body">
                  <h5 className="card-title">Hot Desk</h5>
                  <p className="card-text">Monthly Basis</p>
                  <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
                       Book the seat
                    </Button>
                </div>
              </div>
            </div>
          </div>

          {/*.................... alert dialog start here .............................. */}

          <Dialog
          style={{margin:100}}
          open={this.state.open}
          onClose={this.handleClose}
          TransitionComponent={Transition}
        >
        
          <div className="modal-content">
              <div className="modal-header">
                       <h5 className="modal-title">Address : 01 Kogilu, Mittiganahalli cross Yelahanka, Bangalore 560064</h5>
               
              </div> 

              <div className="modal-header">
                  <h5>Tel : +91 9108896655 </h5>
              </div>   
              
          </div>
        </Dialog>


        </section>


    </div>
    );
  }
}

export default Colors;
