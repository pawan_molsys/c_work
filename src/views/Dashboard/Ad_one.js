import React from 'react';
import { VerticalTimeline, VerticalTimelineElement,MDBTimelineStep }  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import slide0 from '../../assets/img/brand/slide.jpg'
import slide1 from '../../assets/img/brand/slide1.jpg'
import slide2 from '../../assets/img/brand/slide2.jpg'
import slide3 from '../../assets/img/brand/slide3.jpg'
import slide4 from '../../assets/img/brand/slide3.jpg'
import { red, blue } from '@material-ui/core/colors';
import technicalSupport from '../../assets/img/brand/technical-support.png';
import Support from '../../assets/img/brand/support.png';
import desktop from '../../assets/img/brand/desktop.png';
import technical from '../../assets/img/brand/technology.png';

// import { ReactComponent as Img1 } from '../../assets/img/brand/slide1';


const images = [
  {
    link: '/theme/colors',
    url: slide0,
    title: 'Hot Desk',
    width: '40%',
  },
  {
    link:'/theme/typography',
    url: slide1,
    title: 'Dedicated Desk',
    width: '30%',
  },
  {
    link:'/base/breadcrumbs',
    url: slide2,
    title: 'Private Cabin',
    width: '30%',
  },
  {
    link: '/base/cards',
    url: slide3,
    title: 'Private Office',
    width: '40%',
  },
  {
    link: '/base/cards',
    url: slide4,
    title: '',
    width: '30%',
  },
  {
    link: '/base/cards',
    url: slide1,
    title: 'Conference Rooms',
    width: '30%',
  },
];

const Ad_on = () => {

    
    return(
        <div >
           <hr></hr>
          <h3 style={{textAlign:"center", marginTop:10}}> Add Ons</h3>
        <hr></hr>
        <VerticalTimeline>
        
  <VerticalTimelineElement
    className="vertical-timeline-element--work"
    // date="2011 - present"
    iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
    // icon={<slide0/>}
  >
    <div className="card">


        <div className="view overlay">
          <img className="card-img-top" src={Support} alt="Card image cap"/>
          <a>
            <div className="mask rgba-white-slight"></div>
          </a>
        </div>


        <div className="card-body elegant-color white-text rounded-bottom">

          {/* <a className="activator waves-effect mr-4"><i className="fas fa-share-alt white-text"></i></a> */}

          <h4 className="card-title">HR & Operation Support</h4>
          <hr className="hr-light"/>

          {/* <p className="card-text white-text mb-4">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}

          {/* <a href="#!" className="white-text d-flex justify-content-end"><h5>Read more </h5></a> */}

        </div>

      </div>
    
  </VerticalTimelineElement>
  <VerticalTimelineElement
    className="vertical-timeline-element--work"
    // date="2010 - 2011"
    iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
    // icon={<Img1 />}
  >

<div className="card">


<div className="view overlay">
  <img className="card-img-top" src={technicalSupport} alt="Card image cap"/>
  <a>
    <div className="mask rgba-white-slight"></div>
  </a>
</div>


        <div className="card-body elegant-color white-text rounded-bottom">

          {/* <a className="activator waves-effect mr-4"><i className="fas fa-share-alt white-text"></i></a> */}

          <h4 className="card-title">Technical & Co-branding Support</h4>
          <hr className="hr-light"/>

          {/* <p className="card-text white-text mb-4">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}

          {/* <a href="#!" className="white-text d-flex justify-content-end"><h5>Read more </h5></a> */}

        </div>

        </div>
    
  </VerticalTimelineElement>
  <VerticalTimelineElement
    className="vertical-timeline-element--work"
    // date="2008 - 2010"
    iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
    // icon={slide2}
  >

<div className="card">


<div className="view overlay">
  <img className="card-img-top" src={desktop} alt="Card image cap" />
  <a>
    <div className="mask rgba-white-slight"></div>
  </a>
</div>


      <div className="card-body elegant-color white-text rounded-bottom">

        {/* <a className="activator waves-effect mr-4"><i className="fas fa-share-alt white-text"></i></a> */}

        <h4 className="card-title">Desktop, Projector & Acessories</h4>
        <hr className="hr-light"/>

        {/* <p className="card-text white-text mb-4">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}

        {/* <a href="#!" className="white-text d-flex justify-content-end"><h5>Read more </h5></a> */}

      </div>

      </div>
    
  </VerticalTimelineElement>
  <VerticalTimelineElement
    className="vertical-timeline-element--work"
    // date="2006 - 2008"
    iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
    // icon={slide3}
  >

<div className="card">


<div className="view overlay">
  <img className="card-img-top" src={technical} alt="Card image cap"/>
  {/* <a>
    <div className="mask rgba-white-slight"></div>
  </a> */}
</div>


        <div className="card-body elegant-color white-text rounded-bottom">

          {/* <a className="activator waves-effect mr-4"><i className="fas fa-share-alt white-text"></i></a> */}

          <h4 className="card-title">Technology Consulting</h4>
          <hr className="hr-light"/>

          {/* <p className="card-text white-text mb-4">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}

          {/* <a href="#!" className="white-text d-flex justify-content-end"><h5>Read more </h5></a> */}

        </div>

        </div>
   
  </VerticalTimelineElement>
  {/* <VerticalTimelineElement
    className="vertical-timeline-element--education"
    // date="April 2013"
    iconStyle={{ background: 'rgb(233, 30, 99)', color: '#fff' }}
    // icon={slide2}
  >

<div className="card">


<div className="view overlay">
  <img className="card-img-top" src={slide0} alt="Card image cap"/>
  <a>
    <div className="mask rgba-white-slight"></div>
  </a>
</div>


        <div className="card-body elegant-color white-text rounded-bottom">

          <a className="activator waves-effect mr-4"><i className="fas fa-share-alt white-text"></i></a>

          <h4 className="card-title">Card title</h4>
          <hr className="hr-light"/>

          <p className="card-text white-text mb-4">Some quick example text to build on the card title and make up the bulk of the card's content.</p>

          <a href="#!" className="white-text d-flex justify-content-end"><h5>Read more <i className="fas fa-angle-double-right"></i></h5></a>

        </div>

        </div>
  
  </VerticalTimelineElement>
  <VerticalTimelineElement
    className="vertical-timeline-element--education"
    // date="November 2012"
    iconStyle={{ background: 'rgb(233, 30, 99)', color: '#fff' }}
    // icon={slide1}
  >

<div className="card">


<div className="view overlay">
  <img className="card-img-top" src={slide0} alt="Card image cap"/>
  <a>
    <div className="mask rgba-white-slight"></div>
  </a>
</div>


      <div className="card-body elegant-color white-text rounded-bottom">

        <a className="activator waves-effect mr-4"><i className="fas fa-share-alt white-text"></i></a>

        <h4 className="card-title">Card title</h4>
        <hr className="hr-light"/>

        <p className="card-text white-text mb-4">Some quick example text to build on the card title and make up the bulk of the card's content.</p>

        <a href="#!" className="white-text d-flex justify-content-end"><h5>Read more <i className="fas fa-angle-double-right"></i></h5></a>

      </div>

      </div>
   
  </VerticalTimelineElement>
  <VerticalTimelineElement
    className="vertical-timeline-element--education"
    // date="2002 - 2006"
    iconStyle={{ background: 'rgb(233, 30, 99)', color: '#fff' }}
    // icon={slide0}
  >

<div className="card">


<div className="view overlay">
  <img className="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Work/4-col/img%20%2821%29.jpg" alt="Card image cap"/>
  <a>
    <div className="mask rgba-white-slight"></div>
  </a>
</div>


      <div className="card-body elegant-color white-text rounded-bottom">

        <a className="activator waves-effect mr-4"><i className="fas fa-share-alt white-text"></i></a>

        <h4 className="card-title">Card title</h4>
        <hr className="hr-light"/>

        <p className="card-text white-text mb-4">Some quick example text to build on the card title and make up the bulk of the card's content.</p>

        <a href="#!" className="white-text d-flex justify-content-end"><h5>Read more <i className="fas fa-angle-double-right"></i></h5></a>

      </div>

      </div>`

  </VerticalTimelineElement>
   */}
</VerticalTimeline>
</div>
    )
}
export default Ad_on;